const jsonServer = require("json-server");
const auth = require("json-server-auth");

const app = jsonServer.create();
const router = jsonServer.router("db.json");
const port = process.env.PORT || 3001;

app.db = router.db;

const rules = auth.rewriter({
  users: 600,
  platinum: 640,
  games: 644,
});

app.use(rules);
app.use(auth);
app.use(router);
app.listen(port);

console.log("server on: 3001");
